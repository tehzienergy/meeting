$('.menu__btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('menu__btn--active');
  $('.menu__btn-title').toggleClass('menu__btn-title--active');
  $('.menu__content').toggleClass('menu__content--active');
});

$('.menu__link').click(function() {
  $('.menu__content').removeClass('menu__content--active');
  $('.menu__btn').removeClass('menu__btn--active');
  //$('.menu__link').removeClass('menu__link--active');
  //$(this).addClass('menu__link--active');
  $('html, body').animate({
    scrollTop: $( $.attr(this, 'href') ).offset().top - 60
  }, 1000);
});