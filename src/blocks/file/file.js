$('.file__input').fileupload({
    progressall: function (e, data) {
      $(this).closest('.file').find('.file__btn').addClass('file__btn--inactive');
      $(this).closest('.file').find('.file__progress').addClass('file__progress--active');
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $(this).closest('.file').find('.file__bar').css(
          'width',
          progress + '%'
      );
    },
    done: function (e, data) {
      $(this).closest('.file').find('.file__progress').hide();
      $(this).closest('.file').find('.file__final-wrapper').addClass('file__final-wrapper--active');
      var preview = $(this).closest('.file').find('.file__final');
      if (data.files && data.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          preview.attr('src', e.target.result);
        }
        reader.readAsDataURL(data.files[0]);
        data.submit();
      }
    },
});

$('.file__remove').click(function(e) {
  e.preventDefault();
  $(this).closest('.file').find('.file__final-wrapper').removeClass('file__final-wrapper--active');
  $(this).closest('.file').find('.file__btn').removeClass('file__btn--inactive');
  $(this).closest('.file').find('.file__bar').css('width', '0%');
});