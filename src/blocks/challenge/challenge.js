$('.challenge__content').click(function(e) {
  e.preventDefault();
  $('.challenge').removeClass('challenge--active');
  $(this).closest('.challenge').toggleClass('challenge--active');
});

$('.challenge__popup-close').click(function(e) {
  e.preventDefault();
  $('.challenge').removeClass('challenge--active');
});