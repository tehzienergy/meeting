$('.select--subject').select2({
  placeholder: 'Субъект Российской Федерации',
  minimumResultsForSearch: -1,
});

$('.select--role').select2({
  placeholder: 'Роль в СМУС',
  minimumResultsForSearch: -1,
});

$('.select--region').select2({
  placeholder: 'Регион проживания',
  minimumResultsForSearch: -1,
});

$('.select--age').select2({
  placeholder: 'Средний возраст участников',
  minimumResultsForSearch: -1,
});

$('.select--auditory').select2({
  placeholder: 'Целевая аудитория проекта',
  minimumResultsForSearch: -1,
});