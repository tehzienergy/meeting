$('.registration__input').change(function() {
  $(this).addClass('registration__input--active');
  if( !$(this).val() ) {
    $(this).removeClass('registration__input--active');
  }
});

$('.registration__input').each(function() {
  if ($(this).val() != '' || $(this).attr('placeholder') != '') {
    $(this).addClass('registration__input--active');
  }
});

$('.registration .custom-radio--btn .custom-control-input').change(function() {
  if ($(this).is(':checked')) {
    $('.registration__content').addClass('registration__content--active');
  }
});

function textAreaAdjust(o) {
  o.style.height = "1px";
  o.style.height = (7+o.scrollHeight)+"px";
}