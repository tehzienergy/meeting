$('.question__link').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('question__link--active');
  $(this).closest('.question').find('.question__answer').slideToggle('fast');
});